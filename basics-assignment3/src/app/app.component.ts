import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [`
  .five-and-above {
      background-color: blue;  
      color: white;
  }
  `]
})
export class AppComponent {
  displayMessage:boolean = false;
  buttonClicks = [];

  onButtonClick(){
    if(this.displayMessage === true)
      this.displayMessage = false;
    else
      this.displayMessage = true;
    
    this.buttonClicks.push(new Date());
  }
}
