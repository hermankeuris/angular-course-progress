import { Component } from '@angular/core'

@Component({
    selector: 'app-warningalert',
    template: `
    <h2>Warning!</h2>
    <h3>Take note!</h3>
    `,
    styles: [`
        h2{
            color: red;
        }
    `]
})
export class WarningAlert{

}