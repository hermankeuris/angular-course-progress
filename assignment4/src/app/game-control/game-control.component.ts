import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent{
  @Output() intervalEmitted = new EventEmitter<number>();
  interval;
  counter: number = 0;

  onStartGame(){
    this.interval = setInterval(() => {
      this.intervalEmitted.emit(this.counter);
      this.counter++;
    }, 1000)
  }

  onStopGame(){
    clearInterval(this.interval);
  }
}
