import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
  pure: false
})
export class SortPipe implements PipeTransform {

  transform(value: any, sortProperty: string): any {
    return value.sort((x, y) => {
      if(x[sortProperty] > y[sortProperty])
        return 1;
      else if(x[sortProperty] == y[sortProperty])
        return 0;
      else
        return -1
    });
  }

}
