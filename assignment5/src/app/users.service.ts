import { CounterService } from './counter.service';
import { Injectable } from "@angular/core";

@Injectable()
export class UsersService{
    activeUsers = ['Max', 'Anna'];
    inactiveUsers = ['Chris', 'Manu'];

    constructor(private counter: CounterService){}

    getActiveUsers(){
        return this.activeUsers;
    }

    getInactiveUsers(){
        return this.inactiveUsers;
    }

    setToActive(index: number, currentlyActive: boolean){
        if(currentlyActive){
            this.inactiveUsers.push(this.activeUsers[index]);
            this.activeUsers.splice(index, 1);
            this.counter.incInactiveToActive();
        }
        else{
            this.activeUsers.push(this.inactiveUsers[index]);
            this.inactiveUsers.splice(index, 1);
            this.counter.incActiveToInactive();
        }
    }
}