export class CounterService {
    activeToInactiveCounter = 0;
    inactiveToActiveCounter = 0;

    incActiveToInactive(){
        this.activeToInactiveCounter++;
        console.log('From active to inactive: ' + this.activeToInactiveCounter);
    }

    incInactiveToActive(){
        this.inactiveToActiveCounter++;
        console.log('From inactive to active: ' + this.inactiveToActiveCounter);
    }
}