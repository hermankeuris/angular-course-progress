import { UsersService } from './../users.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-active-users',
  templateUrl: './active-users.component.html',
  styleUrls: ['./active-users.component.css']
})
export class ActiveUsersComponent {
  users: string[];
  
  ngOnInit(){
    this.users = this.usersService.getActiveUsers();
  }

  constructor(private usersService: UsersService){}

  onSetToInactive(id: number) {
    this.usersService.setToActive(id, true);
  }
}
