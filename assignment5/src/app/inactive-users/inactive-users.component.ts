import { UsersService } from './../users.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-inactive-users',
  templateUrl: './inactive-users.component.html',
  styleUrls: ['./inactive-users.component.css']
})
export class InactiveUsersComponent {
  users: string[];

  ngOnInit(){
    this.users = this.usersService.getInactiveUsers();
  }

  constructor(private usersService: UsersService){}

  onSetToActive(id: number) {
    this.usersService.setToActive(id, false);
  }
}
