import { Component, OnInit } from '@angular/core';
import * as firebase from "firebase";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  loadedFeature: string = 'recipe';

  ngOnInit(){
    firebase.initializeApp({
      apiKey: "AIzaSyBJpnJTOAFWt34EJ1dQp3rbA1nN4gqZ8gQ",
      authDomain: "ng-recipe-book-hk.firebaseapp.com"
    });
  }

  onNavigate(feature: string){
    this.loadedFeature = feature;
  }
}
