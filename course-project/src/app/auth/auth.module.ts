import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { SigninComponent } from "./signin/signin.component";
import { SignupComponent } from "./signup/signup.component";
import { SignoffComponent } from "./signoff/signoff.component";

import { AuthRoutingModule } from "./auth-routing.module";

@NgModule({
    declarations: [
        SigninComponent,
        SignupComponent,
        SignoffComponent
    ],
    imports: [
        FormsModule,
        AuthRoutingModule
    ]
})
export class AuthModule{

}